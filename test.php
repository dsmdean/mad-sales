					if(isset($_SESSION['subbev'])) {
						if($_SESSION['subbev'] == "salesman") {?>
							<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo $_SESSION["gebnaam"];?></h1>
							<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
								Uw bevoegdheid is: <?php echo $_SESSION['subbev'];?>.
							</p>
							<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
								<tr>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="zoekResultaat.php">Bestellingen Plaatsen</a></td>
								</tr>
							</table>
						<?php
						} else if($_SESSION['subbev'] == "secretaris") {?>
							<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo $_SESSION["gebnaam"];?></h1>
							<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
								Uw bevoegdheid is: <?php echo $_SESSION['subbev'];?>.
							</p>
							<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
								<tr>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertProduct.php">Producten Plaatsen</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="grootGebr.php">Gebruikers Bekijken</a></td>
								</tr>
							</table>
						<?php
						}
					} else {?>
						<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo $_SESSION["gebnaam"];?></h1>
						<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
							Uw bevoegdheid is: <?php echo $_SESSION["bev"];?>.
						</p>
						<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
							<tr>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertProduct.php">Producten Plaatsen</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="zoekResultaat.php">Bestellingen Plaatsen</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertSubUser.php">Creeer gebruiker</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="grootGebr.php">Gebruikers Bekijken</a></td>
							</tr>
						</table>
					<?php
					}