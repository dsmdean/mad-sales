<?php
	session_start();
	
	include "db.php"; 
	
	if(isset($_POST["insertSubmit"])) {
		$iduser = $_SESSION['iduser'];
		$gebnaam = $_SESSION['gebnaam'];
		
		//mysql_connect("localhost","root","admin");
		//mysql_select_db("sales");
		
		$insProdnum = $_POST["productnum"];
		$insNaam = $_POST["naam"];
		$insOmschr = $_POST["omschrijving"];
		$insPrijs = $_POST["prijs"];
		$insVerp = $_POST["verpakkingsmoddel"];
		
		 $target = "img/users/".$gebnaam."/producten_foto/"; 
		 $target = $target . basename( $_FILES['foto']['name']); 
		 $insFoto= $_FILES['foto']['name'];
		 
		 if(move_uploaded_file($_FILES['foto']['tmp_name'], $target)) 
		 { 
		 } 
		 else { 
		 echo "Sorry, there was a problem uploading your file."; 
		 } 
	
		$data = mysql_query("INSERT INTO producten (iduser, productnum, naam, omschrijving, prijs, verpakkingsmodel, foto) VALUES ('$iduser', '$insProdnum', '$insNaam', '$insOmschr', '$insPrijs', '$insVerp', '$insFoto')");
	
		mysql_close();
		header("location:bekijkProduct.php");
	}
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
			
			function reset() {
				document.getElementById("insert").reset();
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<?php
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
				?>
			<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Plaats product als <?php echo $_SESSION["gebnaam"];?></h1>
			<p>
			<center>
				<form action="insertProduct.php" method="POST" id="insert" enctype="multipart/form-data">
					<table width="200" border="0">
						<tr>
							<td>Product nummer</td>
							<td><input type="text" name="productnum" placeholder="Product nummer"></td>
						</tr>
						<tr>
							<td>Naam</td>
							<td><input type="text" name="naam" placeholder="Naam"></td>
						</tr>
						<tr>
							<td>Omschrijving</td>
							<td><input type="text" name="omschrijving" placeholder="Omschrijving"></td>
						</tr>
						<tr>
							<td>Prijs</td>
							<td><input type="text" name="prijs" placeholder="Prijs"></td>
						</tr>
						<tr>
							<td>Verpakkingsmodel</td>
							<td><input type="text" name="verpakkingsmoddel" placeholder="Verpakkingsmodel"></td>
						</tr>
						<tr>
							<td>Foto</td>
							<td><input type="file" name="foto"></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="button" onclick="reset()" value="Reset">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="insertSubmit" value="Submit"></td>
						</tr>
					</table>
				</form>
			</center>
			</p>
			<?php
				}
				else { 
				?>
			<h1>Nog niet ingelogd.</h1>
			<hr />
			<p>
				U moet <a href="index.php"><input type="button" value="inloggen"></a> om deze pagina te bekijken.<br />
			</p>
			<?php
				}
				?>
			<footer>
				<center>
					<p>
						<?php
							if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
							?>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="home.php"><input type="button" value="Terug"></a>
						<?php }?>
						<?php include "footer.php"; ?>
				</center>
			</footer>
		</div>
	</body>
</html>