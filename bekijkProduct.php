<?php
	session_start(); 
	
	include "db.php"; 
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<?php
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
					$id = $_SESSION['iduser'];
				
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					$data = mysql_query("SELECT * FROM producten WHERE iduser = '$id'");
				?>
			<h1>Producten van <?php echo $_SESSION["gebnaam"];?></h1>
			<?php while($info = mysql_fetch_array( $data )) {?>
			<div id = "acts">
				<img src= "img/users/<?php if(isset($_SESSION['subbev'])) { echo $_SESSION['fotofile']; } else { echo $_SESSION['gebnaam']; }?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam']);?></h1>
					</center>
					<p>
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: <?php echo "SRD " . $info['prijs'];?><br/>
						Verpakkingsmoddel: <?php echo $info['verpakkingsmodel'];?>
					</p>
				</div>
			</div>
			<?php }?>
			<?php
				}
				else { 
				?>
			<p>
				U moet <a href="index.php"><input type="button" value="inloggen"></a> om deze pagina te bekijken.<br />
			</p>
			<?php
				}
				?>
			<footer>
				<center>
					<p>
						<?php
							if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {?>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="insertProduct.php"><input type="button" value="Product Plaatsen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="home.php"><input type="button" value="Terug"></a>
						<?php }?>	
						<?php include "footer.php"; ?>
					</p>
				</center>
			</footer>
		</div>
	</body>
</html>