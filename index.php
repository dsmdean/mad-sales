<?php
	session_start();
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Login | Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder" style="height:100%;">
			<?php include "nav.php"; ?>	
			<?php
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass'])) {?>
			<h1>Ingelogt:</h1>
			<p>U bent nu ingelogd als <strong><?php echo $_SESSION["gebnaam"]; ?></strong></p>
			<hr />
			<div id ="inlogform">
				<center>
					<form action="script.php" method="POST">
						<table width="200" border="0">
							<tr>
								<td scope="row" style ="background-color: #b3b3b3;">Inlognaam</td>
								<td><input name="naam" type="textfield" /></td>
							</tr>
							<tr>
								<td scope="row" style ="background-color: #b3b3b3;">Wachwoord</td>
								<td style ="background-color: #b3b3b3;"><input name="pass" type="password" /></td>
							</tr>
							<tr>
								<td style ="background-color: #b3b3b3;">&nbsp;</td>
								<td style ="background-color: #b3b3b3;"></td>
							</tr>
							<tr>
								<td style ="background-color: #b3b3b3;"><input name="" type="submit" value="" style="background: url('img/login.jpg') no-repeat scroll 0 0;width: 100px;height: 29px; color: transparent;" /></td>
								<td style ="background-color: #b3b3b3;"></td>
							</tr>
						</table>
					</form>
				</center>
			</div>
			<h3>U wordt verwezen naar de geheime pagina in 5 seconden.</h3>
			<?php 
				if($_SESSION['bev'] == "groothandelaar") {
					header("location:home.php");
				} else if($_SESSION['bev'] == "retailer") {
					header("location:home2.php");
				} else {
					header("location:home.php");
				}
				} else {?>
			<div id ="inlogform">
				<center>
					<form action="script.php" method="POST">
						<h1>Inlog</h1>
						<table width="200" border="0">
							<tr>
								<td scope="row" style ="background-color: #b3b3b3; color: white;">Inlognaam</td>
								<td style ="background-color: #b3b3b3;"><input name="naam" type="textfield" /></td>
							</tr>
							<tr>
								<td scope="row" style ="background-color: #b3b3b3;color: white;">Wachwoord</td>
								<td style ="background-color: #b3b3b3;"><input name="pass" type="password" /></td>
							</tr>
							<tr>
								<td style ="background-color: #E6E6E6;">&nbsp;</td>
								<td style ="background-color: #E6E6E6;"></td>
							</tr>
							<tr>
								<td style ="background-color: #E6E6E6;"></td>
								<td style ="background-color: #E6E6E6;"><input name="" type="submit" style="background: url('img/login.jpg') no-repeat scroll 0 0 transparent;width: 100px;height: 29px; color: transparent;" /></td>
							</tr>
						</table>
					</form>
				</center>
			</div>
			<?php
				}?>
			<footer>
				<center><?php include "footer.php"; ?></center>
			</footer>
		</div>
	</body>
</html>