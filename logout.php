<?php
	session_start();
	session_destroy();
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Logged Out</h1>
			<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
				<tr>
					<td style="width: 200px; text-align: center;text-decoration:none; color: white; background-color: darkgray;font-style: italic;font-size: 17px;"> " U bent nu uitgelogd " </td>
				</tr>
				<tr>
					<td style="width: 200px; text-align: center;text-decoration:none; color: white; background-color: darkgray;">U kunt hier eventueel opnieuw <br> <a href="index.php"><input type="button" style="background: url('img/login.jpg') no-repeat scroll 0 0 transparent;width: 100px;height: 29px; color: transparent;margin-top: 10px;"></a></td>
				</tr>
			</table>
			<?php header("location:index.php");?>
			<footer>
				<center><?php include "footer.php"; ?></center>
			</footer>
		</div>
	</body>
</html>