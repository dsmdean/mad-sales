<?php
	session_start(); 
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php";
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
					include "search.php";
					
					if(isset($_SESSION['subbev'])) {
						if($_SESSION['subbev'] == "salesman") {?>
							<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo ucfirst($_SESSION["gebnaam"])." - ".$_SESSION['bedrijfsnaam'];?></h1>
							<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
								Uw bevoegdheid is: <?php echo $_SESSION['subbev'];?>.
							</p>
							<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
								<tr>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="zoekResultaat.php">Bestellingen Plaatsen</a></td>
								</tr>
							</table>
						<?php
						} else if($_SESSION['subbev'] == "secretaris") {?>
							<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo ucfirst($_SESSION["gebnaam"])." - ".$_SESSION['bedrijfsnaam'];?></h1>
							<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
								Uw bevoegdheid is: <?php echo $_SESSION['subbev'];?>.
							</p>
							<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
								<tr>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertProduct.php">Producten Plaatsen</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
									<td style="width: 200px; text-align: center;text-decoration:none;"><a href="grootGebr.php">Gebruikers Bekijken</a></td>
								</tr>
							</table>
						<?php
						}
					} else {?>
						<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Welkom <?php echo $_SESSION["gebnaam"];?></h1>
						<p style="text-align: right;margin-right: 10px;margin-top: -20px;text-transform: uppercase;font-family: arial;font-size: 12px;">
							Uw bevoegdheid is: <?php echo $_SESSION["bev"];?>.
						</p>
						<table style="width: 603px;margin-left: auto;margin-right: auto;height: 150px;margin-top: 10%;margin-bottom: 10%;">
							<tr>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertProduct.php">Producten Plaatsen</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bekijkProduct.php">Producten Bekijken</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="bestellingen.php">Bestellingen Bekijken</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="zoekResultaat.php">Bestellingen Plaatsen</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="insertSubUser.php">Creeer gebruiker</a></td>
								<td style="width: 200px; text-align: center;text-decoration:none;"><a href="grootGebr.php">Gebruikers Bekijken</a></td>
							</tr>
						</table>
					<?php
					}
				}
				else { 
				?>
			<h1>Nog niet ingelogd.</h1>
			<hr />
			<p>
				U moet <a href="index.php"><input type="button" value="inloggen" ></a> en de juiste bevoegdheid hebben om deze pagina te bekijken.<br />
			</p>
			<?php
				}
				?>
			<footer>
				<center>
					<p>
						<a href="logout.php"><input type="button" style="background: url('img/logOut.jpg') no-repeat scroll 0 0 transparent;width: 100px;height: 29px;"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="profiel.php"><input type="button" style="background: url('img/prof.jpg') no-repeat scroll 0 0 transparent;width: 100px;height: 29px;"></a>
						<?php include "footer.php";?>
					</p>
				</center>
			</footer>
		</div>
	</body>
</html>