# README #

### What is this repository for? ###

This was a school project.
It was supposed to be a website for wholesalers and retailers.

The retailers should be able to

* view all the wholesalers
* view all of the products of the wholesalers
* place orders
* view those orders

The wholesalers should be able to

* make users for their business
* view all the orders that are coming in
* view the details of those orders
* edit the status of those orders
* view their products
* edit/place products


### How do I get set up? ###

* Pull files
* Put files on a LAMP server
* In the db folder you will find a sql file
* Import the file into a database
* Change db details in db.php

### This project is in Dutch ###