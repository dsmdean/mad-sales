<?php
	session_start(); 
	
	include "db.php";
	$sub = 0;
	$totaal = 0;
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<?php
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "retailer") {
				include "search.php";
					
					if(isset($_GET['add'])) {
						$_SESSION['cart_'.(int)$_GET['add']]+= '1';
						header('Location: viewCart.php');
					}
					
					if(isset($_GET['remove'])) {
						$_SESSION['cart_'.(int)$_GET['remove']]--;
						header('Location: viewCart.php');
					}
					
					if(isset($_GET['delete'])) {
						$_SESSION['cart_'.(int)$_GET['delete']] = '0';
						header('Location: viewCart.php');
					}?>.
			<br/>
			<?php
				foreach($_SESSION as $name => $value) {
					if($value>0) {
						if(substr($name, 0, 5) == 'cart_') {
							$id = substr($name, 5, (strlen($name)-5));
							$get = mysql_query('SELECT * FROM producten WHERE idprod='.mysql_real_escape_string((int)$id).' ORDER BY iduser');
							while ($get_row = mysql_fetch_assoc($get)) {
								$sub = $get_row['prijs']*$value;
								$iduser = $get_row['iduser'];
								$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
								$infob = mysql_fetch_array( $datab );
								echo '<div id = "acts">';?>
			<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $get_row['foto'];?>" width="130" height="130"/><?php
				echo '<div id = "text">';
					echo '<center><h1>'.ucfirst($get_row['naam']).' van '.$infob['naam'].'</h1></center>';
					echo '<p style="margin-top: -2%;">';
					echo 'Product nummer: '.$get_row['productnum'].'<br/>';
					echo 'Omschrijving: '.$get_row['omschrijving'].'<br/>';
					echo 'Verpakkingsmodel: '.$get_row['verpakkingsmodel'].'<br/>';
					echo 'Aantal: '.$value.'<br/>';
					echo '<strong>Subtotaal: '.$value.' x '.ucfirst($get_row['naam']).' @ SRD '.$get_row['prijs'].' = SRD '.$sub.'</strong> <a href="viewcart.php?add='.$id.'"><input type="button" value="[+]"></a> <a href="viewcart.php?remove='.$id.'"><input type="button" value="[ - ]"></a> <a href="viewcart.php?delete='.$id.'"><input type="button" value="Delete"></a><br/>';
					echo '</p>';
				echo '</div>';
				echo '</div>';
				}
				}
				$totaal += $sub;
				}
				}
				if ($totaal == 0) {
				echo '<div id = "acts">';
				echo '<img src= "img/imgtum.jpg"/>';
				echo '<div id = "text">';
				echo '<center><h1>U hebt niets besteld.</h1></center>';
				echo '<p style="margin-top: -2%;">';
				echo '</p>';
				echo '</div>';
				echo '</div>';
				} else {
				echo '<div id = "acts">';
				echo '<div id = "text">';
				echo '<center><h1>Totaal</h1></center>';
				echo '<p style="margin-top: -2%;">';
				echo 'Totaal: SRD '.number_format($totaal, 2);
				echo '<br/><a href="checkout.php">Checkout</a>';
				echo '</p>';
				echo '</div>';
				echo '</div>';
				
				$_SESSION['prijs'] = $totaal;
				}
				}
				else if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
				if(!isset($_SESSION['idretailer'])) {
					header('Location: insertStraat.php');
				}
				include "search.php";
				
				if(isset($_GET['add'])) {
				$_SESSION['cart_'.(int)$_GET['add']]+= '1';
				header('Location: viewCart.php');
				}
				
				if(isset($_GET['remove'])) {
				$_SESSION['cart_'.(int)$_GET['remove']]--;
				header('Location: viewCart.php');
				}
				
				if(isset($_GET['delete'])) {
				$_SESSION['cart_'.(int)$_GET['delete']] = '0';
				header('Location: viewCart.php');
				}?>.
			<br/>
			<?php
				$idretailer = $_SESSION['idretailer'];
				$query_retailer_kiezen = mysql_query("SELECT * FROM users WHERE iduser='$idretailer'");
				$result_retailer_kiezen = mysql_fetch_array($query_retailer_kiezen);
				?>
				<h3>U besteld producten voor <?php echo $result_retailer_kiezen['naam'];?>. <a href="insertStraat.php?verander=true&cat=view">Veranderen</a></h3><?php
				foreach($_SESSION as $name => $value) {
					if($value>0) {
						if(substr($name, 0, 5) == 'cart_') {
							$id = substr($name, 5, (strlen($name)-5));
							$get = mysql_query('SELECT * FROM producten WHERE idprod='.mysql_real_escape_string((int)$id));
							while ($get_row = mysql_fetch_assoc($get)) {
								$sub = $get_row['prijs']*$value;
								$iduser = $get_row['iduser'];
								$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
								$infob = mysql_fetch_array( $datab );
								echo '<div id = "acts">';?>
			<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $get_row['foto'];?>" width="130" height="130"/><?php
				echo '<div id = "text">';
					echo '<center><h1>'.ucfirst($get_row['naam']).'</h1></center>';
					echo '<p style="margin-top: -2%;">';
					echo 'Product nummer: '.$get_row['productnum'].'<br/>';
					echo 'Omschrijving: '.$get_row['omschrijving'].'<br/>';
					echo 'Verpakkingsmodel: '.$get_row['verpakkingsmodel'].'<br/>';
					echo 'Aantal: '.$value.'<br/>';
					echo '<strong>Subtotaal: '.$value.' x '.ucfirst($get_row['naam']).' @ SRD '.$get_row['prijs'].' = SRD '.$sub.'</strong> <a href="viewcart.php?add='.$id.'"><input type="button" value="[+]"></a> <a href="viewcart.php?remove='.$id.'"><input type="button" value="[ - ]"></a> <a href="viewcart.php?delete='.$id.'"><input type="button" value="Delete"></a><br/>';
					echo '</p>';
				echo '</div>';
				echo '</div>';
				}
				}
				$totaal += $sub;
				}
				}
				if ($totaal == 0) {
				echo '<div id = "acts">';
				echo '<img src= "img/imgtum.jpg"/>';
				echo '<div id = "text">';
				echo '<center><h1>U hebt niets besteld.</h1></center>';
				echo '<p style="margin-top: -2%;">';
				echo '</p>';
				echo '</div>';
				echo '</div>';
				} else {
				echo '<div id = "acts">';
				echo '<div id = "text">';
				echo '<center><h1>Totaal</h1></center>';
				echo '<p style="margin-top: -2%;">';
				echo 'Totaal: SRD '.number_format($totaal, 2);
				echo '<br/><a href="checkout.php">Checkout</a>';
				echo '</p>';
				echo '</div>';
				echo '</div>';
				
				$_SESSION['prijs'] = $totaal;
				}
				}
				else { 
				header("location:index.php");
				}
				?>
			<footer>
				<center>
					<?php
						if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "retailer") {
						?>
					<p>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="profiel.php"><input type="button" value="Profiel bekijken"></a>
						<?php }?>
						<?php include "footer.php"; ?>
					</p>
				</center>
			</footer>
		</div>
	</body>
</html>