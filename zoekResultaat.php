<?php
	session_start();
	
	include "db.php";
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<?php
				include "search.php";
				//$datacat = mysql_query("SELECT * FROM categorie");
					//while($infocat = mysql_fetch_array( $datacat )) {
						//echo $infocat['categorie'] . ' | ';
					//}
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "retailer") {
					//include "search.php";
					$keySalt = "aghtUJ6y";     // same as used in encryptLink function
					$queryString = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), urldecode(base64_decode($_SERVER['QUERY_STRING'])), MCRYPT_MODE_CBC, md5(md5($keySalt))), "\0");   //this line of code decrypt the query string
					parse_str($queryString);   //parse query string
					if(!empty($zoekProduct)) {
						$product = $prodNaam;
						
						if($cat == "Produkten") {
						$data = mysql_query("SELECT * FROM producten WHERE naam LIKE '%$product%'");
						//$data = mysql_query("SELECT * FROM producten LEFT JOIN users ON producten.iduser = users.iduser WHERE users.idbevoegd = '1'");
						?>
			<center>
				<h2>U hebt gezocht op <?php echo $product;?></h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
					$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
					$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam'])." van ".$infob['naam'];?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				} else if($cat == "Groothandelaar") {
				$data = mysql_query("SELECT * FROM users WHERE naam LIKE '%$product%' and idbevoegd = '1'");
				?>
			<center>
				<h2>U hebt gezocht op <?php echo $product;?></h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
				$grootnaam = $info['naam'];
				function encryptLink($val1, $val2){
					$keySalt = "aghtUJ6y";  // change it
					$qryStr = "grootprodukt=".$val1."&grootnaam=".$val2;  //making query string
					$query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), $qryStr, MCRYPT_MODE_CBC, md5(md5($keySalt)))));    //this line of code encrypt the query string
					$link = "zoekResultaat.php?".$query;
					return $link;
				}
				$link = encryptLink($iduser, $grootnaam);?>
			<a href="<?php echo $link;?>">
				<div id = "acts">
					<img src= "img/imgtum.jpg"/>
					<div id = "text">
						<center>
							<h1><?php echo ucfirst($grootnaam);?></h1>
						</center>
			</a>
			<p style="margin-top: -2%;">
			Datum opgericht: <?php echo $info['opg_datum'];?><br/>
			Plaats: <?php echo $info['plaats'];?><br/>
			Adres: <?php echo $info['adres'];?><br/>
			Datum geregistreerd: <?php echo $info['dat_ger'];?><br/>
			</p>
			</div>
			</div>
			<?php }
				}
				} 
				else if(!empty($grootprodukt)) {
				//mysql_connect("localhost","root","admin");
				//mysql_select_db("sales");
				$iduser = $grootprodukt;
				$data = mysql_query("SELECT * FROM producten WHERE iduser = '$iduser'");
				?>
			<center>
				<h2>U kan de produkten van <?php echo $grootnaam;?> bekijken.</h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
				$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
				$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam'])." van ".$infob['naam'];?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				}
				else if(!empty($categorie)) {
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					$idcategorie = $categorie;
					$data = mysql_query("SELECT * FROM producten WHERE idcat = '$idcategorie'");
					$datac = mysql_query("SELECT * FROM categorie WHERE idcat = '$idcategorie'");
					$infoc = mysql_fetch_array( $datac );
					?>
			<center>
				<h2>U kan de produkten in <?php echo $infoc['categorie'];?> bekijken.</h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
				$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
				$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam'])." van ".$infob['naam'];?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				}
				else {
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					$data = mysql_query("SELECT * FROM producten");
					?>
			<center>
				<h2>U kan de produkten bekijken.</h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
					$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
					$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam'])." van ".$infob['naam'];?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				}
				}
				else if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
				if(!isset($_SESSION['idretailer'])) {
					header('Location: insertStraat.php');
				}
				//include "search.php";
				$idgebruiker = $_SESSION['iduser'];
				$keySalt = "aghtUJ6y";     // same as used in encryptLink function
				$queryString = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), urldecode(base64_decode($_SERVER['QUERY_STRING'])), MCRYPT_MODE_CBC, md5(md5($keySalt))), "\0");   //this line of code decrypt the query string
				parse_str($queryString);   //parse query string
				
				$idretailer = $_SESSION['idretailer'];
				$query_retailer_kiezen = mysql_query("SELECT * FROM users WHERE iduser='$idretailer'");
				$result_retailer_kiezen = mysql_fetch_array($query_retailer_kiezen);
				?>
			<h3>U besteld producten voor <?php echo $result_retailer_kiezen['naam'];?>. <a href="insertStraat.php?verander=true&cat=zoek">Veranderen</a></h3>
			<?php
				if(!empty($zoekProduct)) {
					$product = $prodNaam;
				
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					$data = mysql_query("SELECT * FROM producten WHERE naam LIKE '%$product%' AND iduser = '$idgebruiker'");
					?>
			<center>
				<h2>U hebt gezocht op <?php echo $product;?></h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
					$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
					$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam']);?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				} 
				else {
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					$data = mysql_query("SELECT * FROM producten WHERE iduser = '$idgebruiker'");
					?>
			<center>
				<h2>U kan de produkten bekijken.</h2>
			</center>
			<?php while($info = mysql_fetch_array( $data )) {
				$iduser = $info['iduser'];
					$datab = mysql_query("SELECT * FROM users WHERE iduser = '$iduser'");
					$infob = mysql_fetch_array( $datab );?>
			<div id = "acts">
				<img src= "img/users/<?php echo $infob['gebnaam'];?>/producten_foto/<?php echo $info['foto'];?>" width="130" height="130"/>
				<div id = "text">
					<center>
						<h1><?php echo ucfirst($info['naam']);?></h1>
					</center>
					<p style="margin-top: -2%;">
						Product nummer: <?php echo $info['productnum'];?><br/>
						Omschrijving: <?php echo $info['omschrijving'];?><br/>
						Prijs: SRD <?php echo $info['prijs'];?><br/>
						Verpakkingsmodel: <?php echo $info['verpakkingsmodel'];?><br/>
						<a href="viewCart.php?add=<?php echo $info['idprod'];?>">Add To Cart</a>
					</p>
				</div>
			</div>
			<?php }
				}
				}
				else { 
				header("location:index.php");
				}
				?>		
			<footer>
				<center>
					<p>
						<?php
							if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "retailer") {?>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="home2.php"><input type="button" value="Terug"></a>
						<?php }?>
						<?php include "footer.php"; ?>
					</p>
				</center>
			</footer>
		</div>
	</body>
</html>