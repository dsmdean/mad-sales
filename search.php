<?php
	if(isset($_GET["zoekProductR"])) {
		$zoekProduct = $_GET["zoekProductR"];
		$prodNaam = $_GET["prodNaam"];
		$cat = $_GET["cat"];
		function encryptLink($val1, $val2, $val3){
			$keySalt = "aghtUJ6y";  // change it
			$qryStr = "zoekProduct=".$val1."&prodNaam=".$val2."&cat=".$val3;  //making query string
			$query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), $qryStr, MCRYPT_MODE_CBC, md5(md5($keySalt)))));    //this line of code encrypt the query string
			$link = "zoekResultaat.php?".$query;
			return $link;
		}
		$link = encryptLink($zoekProduct, $prodNaam, $cat);
		
		header("location:$link");
	} else if(isset($_GET["zoekProductG"])) {
		$zoekProduct = $_GET["zoekProductG"];
		$prodNaam = $_GET["prodNaam"];
		function encryptLink($val1, $val2){
			$keySalt = "aghtUJ6y";  // change it
			$qryStr = "zoekProduct=".$val1."&prodNaam=".$val2;  //making query string
			$query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), $qryStr, MCRYPT_MODE_CBC, md5(md5($keySalt)))));    //this line of code encrypt the query string
			$link = "zoekResultaat.php?".$query;
			return $link;
		}
		$link = encryptLink($zoekProduct, $prodNaam);
		
		header("location:$link");
	} else if(isset($_GET["catR"])) {
		$categorie = $_GET["categorie"];
		function encryptLink($val1){
			$keySalt = "aghtUJ6y";  // change it
			$qryStr = "categorie=".$val1;  //making query string
			$query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), $qryStr, MCRYPT_MODE_CBC, md5(md5($keySalt)))));    //this line of code encrypt the query string
			$link = "zoekResultaat.php?".$query;
			return $link;
		}
		$link = encryptLink($categorie);
		
		header("location:$link");
	}
	
	if($_SESSION['bev'] == "retailer") {
	?>
<div id="tfheader">
	<form id="tfnewsearchl" method="GET" action="zoekResultaat.php">
		<select id="tfq" class="tftextinput4" name="categorie">
			<option value="All">Alle departementen</option>
			<?php 
				$query_cat = "SELECT * FROM categorie";
				$result_cat = mysql_query($query_cat);
				while($data_cat= mysql_fetch_array($result_cat)) {?>
			<option value="<?php echo $data_cat['idcat'];?>"><?php echo $data_cat['categorie'];?></option>
			<?php }?>
		</select>
		<input type="submit" name="catR" value="" class="tfbutton4">
	</form>
	<form id="tfnewsearch" method="GET" action="search.php">
		<input type="text" id="tfq" class="tftextinput4" name="prodNaam" size="21" maxlength="120" value="Search">
		<select id="tfq" class="tftextinput4" name="cat">
			<option value="Produkten">Produkten</option>
			<option value="Groothandelaar">Groothandelaar</option>
		</select>
		<input type="submit" name="zoekProductR" value=" " class="tfbutton4">
	</form>
	<div class="tfclear"></div>
</div>
<?php
	} else if($_SESSION['bev'] == "groothandelaar") {?>
<div id="tfheader">
	<form id="tfnewsearch" method="GET" action="zoekResultaat.php">
		<input type="text" id="tfq" class="tftextinput4" name="prodNaam" size="21" maxlength="120" value="Search">
		<input type="submit" name="zoekProductG" value=" " class="tfbutton4">
	</form>
	<div class="tfclear"></div>
</div>
<?php
	}
	?>