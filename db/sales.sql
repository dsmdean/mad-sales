-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2016 at 04:57 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `bestellingen`
--

CREATE TABLE IF NOT EXISTS `bestellingen` (
  `idbestell` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totaal_prijs` varchar(45) NOT NULL,
  PRIMARY KEY (`idbestell`),
  KEY `retailers_idx` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `bestellingen`
--

INSERT INTO `bestellingen` (`idbestell`, `iduser`, `datum`, `totaal_prijs`) VALUES
(44, 2, '2014-06-01 17:06:35', '3020'),
(45, 2, '2014-06-01 17:15:00', '30'),
(46, 2, '2014-06-01 17:22:06', '10'),
(47, 2, '2014-06-01 17:23:41', '10'),
(48, 2, '2014-06-25 13:09:33', '24'),
(49, 2, '2014-06-25 13:37:18', '10'),
(50, 1, '2014-06-25 14:03:24', '3'),
(51, 2, '2014-06-25 14:06:50', '20'),
(52, 2, '2014-06-25 14:07:35', '30'),
(53, 2, '2014-06-25 14:08:54', '25'),
(54, 2, '2014-06-25 17:17:14', '12'),
(55, 2, '2014-07-05 17:20:01', '3029'),
(56, 2, '2014-07-05 17:22:19', '10'),
(57, 2, '2014-07-12 14:43:40', '70'),
(58, 2, '2014-07-16 21:44:38', '10'),
(72, 2, '2014-08-04 01:11:58', '10');

-- --------------------------------------------------------

--
-- Table structure for table `bestellingen_det`
--

CREATE TABLE IF NOT EXISTS `bestellingen_det` (
  `idbestel_det` int(11) NOT NULL AUTO_INCREMENT,
  `idbestell` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idproduct` int(11) NOT NULL,
  `aantal` varchar(45) NOT NULL,
  `totaal_prijs` varchar(45) NOT NULL,
  `idstat` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbestel_det`),
  KEY `bestell_idx` (`idbestell`),
  KEY `groothandelaren_idx` (`iduser`),
  KEY `producten_idx` (`idproduct`),
  KEY `status_idx` (`idstat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `bestellingen_det`
--

INSERT INTO `bestellingen_det` (`idbestel_det`, `idbestell`, `iduser`, `idproduct`, `aantal`, `totaal_prijs`, `idstat`) VALUES
(25, 44, 1, 6, '1', '3000', 4),
(26, 44, 1, 7, '1', '10', 4),
(27, 45, 2, 3, '1', '15', 4),
(28, 46, 1, 21, '1', '5', 4),
(29, 47, 1, 21, '1', '5', 4),
(30, 48, 1, 2, '2', '24', 4),
(31, 49, 1, 1, '1', '5', 4),
(32, 50, 1, 4, '3', '3', 4),
(33, 51, 1, 1, '2', '10', 4),
(34, 52, 2, 3, '2', '30', 4),
(35, 53, 1, 1, '1', '5', 4),
(36, 53, 2, 3, '1', '15', 4),
(37, 53, 1, 5, '1', '5', 4),
(38, 54, 1, 2, '1', '12', 4),
(39, 55, 1, 2, '2', '24', 4),
(40, 55, 1, 6, '1', '3000', 4),
(41, 55, 1, 21, '1', '5', 4),
(42, 56, 1, 7, '1', '10', 4),
(43, 57, 1, 5, '3', '15', 4),
(44, 57, 1, 7, '3', '30', 4),
(45, 57, 1, 21, '5', '25', 4),
(46, 58, 1, 1, '1', '5', 2),
(47, 72, 1, 1, '1', '5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bevoegdheden`
--

CREATE TABLE IF NOT EXISTS `bevoegdheden` (
  `idbevoegd` int(11) NOT NULL AUTO_INCREMENT,
  `bevoegdheid` varchar(45) NOT NULL,
  PRIMARY KEY (`idbevoegd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bevoegdheden`
--

INSERT INTO `bevoegdheden` (`idbevoegd`, `bevoegdheid`) VALUES
(1, 'groothandelaar'),
(2, 'retailer');

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `idcat` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(45) NOT NULL,
  PRIMARY KEY (`idcat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`idcat`, `categorie`) VALUES
(1, 'Books & Audible'),
(2, 'Movies, Music & Games'),
(3, 'Electronics & Computers'),
(4, 'Home, Garden & Tools'),
(5, 'Beauty, Health, Grocery'),
(6, 'Toys, Kids & Baby'),
(7, 'Clothing, Shoes & Jewelry'),
(8, 'Sports & Outdoors'),
(9, 'Automotive & Industrial');

-- --------------------------------------------------------

--
-- Table structure for table `producten`
--

CREATE TABLE IF NOT EXISTS `producten` (
  `idprod` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `productnum` varchar(45) NOT NULL,
  `naam` varchar(45) NOT NULL,
  `omschrijving` longtext NOT NULL,
  `prijs` varchar(45) NOT NULL,
  `verpakkingsmodel` longtext NOT NULL,
  `foto` varchar(45) NOT NULL,
  `idcat` int(11) DEFAULT NULL,
  PRIMARY KEY (`idprod`),
  KEY `users_idx` (`iduser`),
  KEY `categorie_idx` (`idcat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `producten`
--

INSERT INTO `producten` (`idprod`, `iduser`, `productnum`, `naam`, `omschrijving`, `prijs`, `verpakkingsmodel`, `foto`, `idcat`) VALUES
(1, 1, '123', 'pindakaas', 'bruin en lekker', '5', 'in fles', 'img.jpg', 1),
(2, 1, '321', 'kaas', 'lekker en geel', '12', 'een wiel', 'img_2.jpg', 9),
(3, 1, '345', 'gehakt', 'vlees', '15', 'in pond', 'img.jpg', 7),
(4, 1, '099', 'pinda', 'nootjes', '1', 'per gram', 'img_2.jpg', 2),
(5, 1, '555', 'koek', 'chocholade', '5', 'per 300 gram', 'img.jpg', 1),
(6, 1, '444', 'laptop', 'dell xps', '3000', 'per laptop', 'img_2.jpg', 3),
(7, 1, '556', 'Rasberry', 'lekkertjes', '10', 'per hoop', 'download.jpg', 3),
(21, 1, '999867', 'ice cream', 'lekker ijs', '5', 'per bak', 'download (1).jpg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`idstatus`, `status`) VALUES
(1, 'nog niet behandelt'),
(2, 'behandelt'),
(3, 'verpakt'),
(4, 'afgeleverd');

-- --------------------------------------------------------

--
-- Table structure for table `subbevoegdheden`
--

CREATE TABLE IF NOT EXISTS `subbevoegdheden` (
  `idsubbevoegd` int(11) NOT NULL AUTO_INCREMENT,
  `subbevoegdheid` varchar(45) NOT NULL,
  PRIMARY KEY (`idsubbevoegd`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subbevoegdheden`
--

INSERT INTO `subbevoegdheden` (`idsubbevoegd`, `subbevoegdheid`) VALUES
(1, 'salesman'),
(2, 'secretaris');

-- --------------------------------------------------------

--
-- Table structure for table `subusers`
--

CREATE TABLE IF NOT EXISTS `subusers` (
  `idsubuser` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `gebnaam` varchar(45) NOT NULL,
  `wachtwoord` varchar(45) NOT NULL,
  `idsubbevoegd` int(11) NOT NULL,
  PRIMARY KEY (`idsubuser`),
  KEY `bedrijf_idx` (`iduser`),
  KEY `subbevoegd_idx` (`idsubbevoegd`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subusers`
--

INSERT INTO `subusers` (`idsubuser`, `iduser`, `gebnaam`, `wachtwoord`, `idsubbevoegd`) VALUES
(1, 1, 'Sale', 'adminroot', 1),
(2, 1, 'secretariaat', 'school', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `idbevoegd` int(11) NOT NULL,
  `naam` varchar(45) NOT NULL,
  `opg_datum` date NOT NULL,
  `plaats` varchar(45) NOT NULL,
  `adres` varchar(45) NOT NULL,
  `dat_ger` date NOT NULL,
  `gebnaam` varchar(45) NOT NULL,
  `wachtwoord` varchar(45) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`iduser`),
  KEY `bevoegdheden_idx` (`idbevoegd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iduser`, `idbevoegd`, `naam`, `opg_datum`, `plaats`, `adres`, `dat_ger`, `gebnaam`, `wachtwoord`, `email`) VALUES
(1, 1, 'Groothandel', '2013-01-29', 'Paramaribo', 'Onoribolaan 44', '2014-03-01', 'sacrament', 'admin', 'dean@beamingart.com'),
(2, 2, 'Retailer', '2014-02-08', 'Paramaribo', 'Franchepanestraat', '2014-03-04', 'bosroko', 'root', 'deansmenso@live.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bestellingen`
--
ALTER TABLE `bestellingen`
  ADD CONSTRAINT `gebruikers` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bestellingen_det`
--
ALTER TABLE `bestellingen_det`
  ADD CONSTRAINT `bestelling` FOREIGN KEY (`idbestell`) REFERENCES `bestellingen` (`idbestell`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `product` FOREIGN KEY (`idproduct`) REFERENCES `producten` (`idprod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `producten`
--
ALTER TABLE `producten`
  ADD CONSTRAINT `cat` FOREIGN KEY (`idcat`) REFERENCES `categorie` (`idcat`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `subusers`
--
ALTER TABLE `subusers`
  ADD CONSTRAINT `bedrijf` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `subbevoegd` FOREIGN KEY (`idsubbevoegd`) REFERENCES `subbevoegdheden` (`idsubbevoegd`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `bevoegdheid` FOREIGN KEY (`idbevoegd`) REFERENCES `bevoegdheden` (`idbevoegd`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
