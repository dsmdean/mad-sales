<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
			
			function reset() {
				document.getElementById("insert").reset();
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>
			<?php
				session_start();
				
				include "db.php"; 
				
				if(!isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
					header("location:index.php");
				}
				
				if(isset($_POST["updateSubmit"])) {
					$idsubuser = $_SESSION['idsubuser'];
					$gebnaam = $_POST['gebruikersnaam'];
					$wachtwoord = $_POST['wachtwoord'];
					$functie = $_POST['functie']; 
					$query_update_gebruiker = "UPDATE `subusers` SET `gebnaam`='$gebnaam',`wachtwoord`='$wachtwoord',`idsubbevoegd`='$functie' WHERE `idsubuser`='$idsubuser'";
					$result_update_gebruiker = mysql_query($query_update_gebruiker);
					mysql_close();
					header("location:grootGebr.php");
				} else if(isset($_POST["insertSubmit"])) {
					$iduser = $_SESSION['iduser'];
					$gebnaam = $_SESSION['gebnaam'];
					
					//mysql_connect("localhost","root","admin");
					//mysql_select_db("sales");
					
					$gebruikersnaam = $_POST["gebruikersnaam"];
					$wachtwoord = $_POST["wachtwoord"];
					$functie = $_POST["functie"];
				
					$data = mysql_query("INSERT INTO subusers (iduser, gebnaam, wachtwoord, idsubbevoegd) VALUES ('$iduser', '$gebruikersnaam', '$wachtwoord', '$functie')");
				
					mysql_close();
					header("location:grootGebr.php");
				} else if(isset($_GET['editSubUser'])) {
					$idsubuser = $_GET['editSubUser'];
					$_SESSION['idsubuser'] = $idsubuser;
					$data = mysql_query("SELECT * FROM subusers INNER JOIN subbevoegdheden ON subusers.idsubbevoegd = subbevoegdheden.idsubbevoegd WHERE idsubuser = '$idsubuser'");
					$info = mysql_fetch_array( $data );?>
					<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Edit gebruiker</h1>
			<p>
			<center>
				<form action="insertSubUser.php" method="POST" id="insert" enctype="multipart/form-data">
					<table width="200" border="0">
						<tr>
							<td>Gebruikersnaam</td>
							<td><input type="text" name="gebruikersnaam" value="<?php echo $info['gebnaam'];?>"></td>
						</tr>
						<tr>
							<td>Wachtwoord</td>
							<td><input type="text" name="wachtwoord" value="<?php echo $info['wachtwoord'];?>"></td>
						</tr>
						<tr>
							<td>Functie</td>
							<td>
								<select name="functie">
									<option value="<?php echo $info['idsubbevoegd'];?>"><?php echo $info['subbevoegdheid'];?></option>
									<?php $datab = mysql_query("SELECT * FROM subbevoegdheden");
									while($infob = mysql_fetch_array( $datab )) { if($info['idsubbevoegd']==$infob['idsubbevoegd']){} else {?>
										<option value="<?php echo $infob['idsubbevoegd'];?>"><?php echo $infob['subbevoegdheid'];?></option>
									<?php }}?>
								</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="updateSubmit" value="Update"></td>
						</tr>
					</table>
				</form>
			</center>
			</p><?php
				} else {
					$data = mysql_query("SELECT * FROM subbevoegdheden");?>
			<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Creeer een nieuwe gebruiker</h1>
			<p>
			<center>
				<form action="insertSubUser.php" method="POST" id="insert" enctype="multipart/form-data">
					<table width="200" border="0">
						<tr>
							<td>Gebruikersnaam</td>
							<td><input type="text" name="gebruikersnaam" placeholder="Gebruikersnaam"></td>
						</tr>
						<tr>
							<td>Wachtwoord</td>
							<td><input type="password" name="wachtwoord"></td>
						</tr>
						<tr>
							<td>Functie</td>
							<td>
								<select name="functie">
									<option></option>
									<?php while($info = mysql_fetch_array( $data )) {?>
										<option value="<?php echo $info['idsubbevoegd'];?>"><?php echo $info['subbevoegdheid'];?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="button" onclick="reset()" value="Reset">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="insertSubmit" value="Submit"></td>
						</tr>
					</table>
				</form>
			</center>
			</p>
			<?php }?>
			<footer>
				<center>
					<p>
						<?php
							if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
							?>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="home.php"><input type="button" value="Terug"></a>
						<?php }?>
						<?php include "footer.php"; ?>
				</center>
			</footer>
		</div>
	</body>
</html>
