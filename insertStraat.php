<?php
	session_start();
	
	if(isset($_SESSION['subbev'])) {
		header('Location: index.php');
	}
	
	if(isset($_GET['verander'])) {
		unset($_SESSION['idretailer']);
		if($_GET['cat'] == "zoek") {
			$_SESSION['grootAlsRetailer'] = "zoek";
		} else if($_GET['cat'] == "view") {
			$_SESSION['grootAlsRetailer'] = "view";
		}
	}
	
	if(isset($_SESSION['idretailer'])) {
		header('Location: zoekResultaat.php');
	}
	
	if(isset($_POST['insertSubmit'])) {
		if(empty($_POST['idretailer'])) {
			
		} else {
			$_SESSION['idretailer'] = $_POST['idretailer'];
			if($_SESSION['grootAlsRetailer'] == "zoek") {
				header("location:zoekResultaat.php");
			} else if($_SESSION['grootAlsRetailer'] == "view") {
				header("location:viewCart.php");
			} else {
				header("location:zoekResultaat.php");
			}
		}
	}
	
	include "db.php";
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript">
			window.onload = function(){ 
				//Get submit button
				var submitbutton = document.getElementById("tfq");
				//Add listener to submit button
				if(submitbutton.addEventListener){
					submitbutton.addEventListener("click", function() {
						if (submitbutton.value == 'Search'){//Customize this text string to whatever you want
							submitbutton.value = '';
						}
					});
				}
			}
			
			function reset() {
				document.getElementById("insert").reset();
			}
		</script>
		<title>Bestel Systeem</title>
	</head>
	<body>
		<div id = "con-holder">
			<?php include "nav.php"; ?>	
			<?php
				if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
				?>
			<h1 style="background-color: darkgray;text-align: center;font-family: arial;">Plaats retailers gegevens</h1>
			<p>
			<center>
				<form action="insertStraat.php" method="POST" id="insert" enctype="multipart/form-data">
					<table width="200" border="0">
						<tr>
							<td>Retailer</td>
							<td>
								<select name="idretailer" id="tfq" class="tftextinput4" required>
									<option value=""></option>
									<?php
										$data = mysql_query("SELECT * FROM users WHERE idbevoegd = '2'");
										while($info = mysql_fetch_array( $data )) {?>
									<option value="<?php echo $info['iduser'];?>"><?php echo $info['naam'];?></option>
									<?php
										}?>
								</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="insertSubmit" value="Submit"></td>
						</tr>
					</table>
				</form>
			</center>
			</p>
			<?php
				}
				else { 
					header("location:index.php");
				}
				?>
			<footer>
				<center>
					<p>
						<?php
							if(isset($_SESSION['gebnaam']) && isset($_SESSION['pass']) && $_SESSION['bev'] == "groothandelaar") {
							?>
						<a href="logout.php"><input type="button" value="Uitloggen"></a>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="home.php"><input type="button" value="Terug"></a>
						<?php }?>
						<?php include "footer.php"; ?>
				</center>
			</footer>
		</div>
	</body>
</html>